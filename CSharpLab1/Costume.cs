﻿namespace CSharpLab1
{
    class Costume
    {
        private int id;
        private string name;
        private float price;
        private string material;
        private string country;

        public Costume(int id,string name,float price,string material,string country)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.material = material;
            this.country = country;           
        }
        public int getId()
        {
            return id;
        }
        public void setId(int id)
        {
            this.id = id;
        }
        public string getName()
        {
            return name;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public float getPrice()
        {
            return price;
        }
        public void setPrice(float price)
        {
            this.price = price;
        }
        public string getMaterial()
        {
            return material;
        }
        public void setMaterial(string material)
        {
            this.material = material;
        }
        public string getCountry()
        {
            return country;
        }
        public void setCountry(string country)
        {
            this.country = country;
        }       

    }
}

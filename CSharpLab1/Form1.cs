﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CSharpLab1
{
    public partial class Form1 : Form
    {
        //Колекция SortedList
        private SortedList<int,Costume> costumesList = new SortedList<int,Costume>();
        //Добавление в колекцию    
        private void addCostume(int id,string name,float price,string material,string country)
        {
            if (costumesList.Keys.Contains(id))
            {
                MessageBox.Show("ID уже занят","Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            Costume c = new Costume(id, name, price, material, country);
            costumesList.Add(c.getId(), c);            
            showList();
        }
        //Удаление с колекции     
        private void deleteCostume(int key)
        {
            costumesList.RemoveAt(key);
            dataGridView1.Rows.RemoveAt(key);
        }
        //Отображение колекции в таблице
        private void showList()
        {
            dataGridView1.Rows.Clear();        
            foreach(Costume c in costumesList.Values){
                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < 5; i++)
                {
                    row.Cells.Add(new DataGridViewTextBoxCell());
                }
                row.Cells[0].Value = c.getId();
                row.Cells[1].Value = c.getName();
                row.Cells[2].Value = c.getPrice();
                row.Cells[3].Value = c.getMaterial();
                row.Cells[4].Value = c.getCountry();                
                dataGridView1.Rows.Add(row);
            }
        }

        public Form1()
        {
            InitializeComponent();        
            startAddTestItems();
        }  

        private void startAddTestItems()
        {           
            addCostume(10023, "Діловий костюм", 5500f, "лен", "Украина");
            addCostume(14334, "Abibas", 4000f, "синтетика", "Китай");
            addCostume(46456, "EliteBoss", 9999f, "лен", "Италия");
            addCostume(67667, "XZ Costume", 5300f, "лен", "Украина");
            addCostume(94234, "Adidas", 7000f, "синтетика", "Польша");

        }
      
        //Обработчик нажатия на кнопку добавления
        private void button1_Click(object sender, EventArgs e)
        {
            int id = -1;
            float price = -1f;
            try
            {
                id = int.Parse(textBox1.Text);
                price = float.Parse(textBox3.Text);
            }
            catch(Exception e1)
            {
                MessageBox.Show("Неправильный ввод числа", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            addCostume(id, textBox2.Text, price, textBox4.Text, textBox5.Text);            
        }    

   
        //Обработчик нажатия на удалить
        private void удалитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            DialogResult dr = MessageBox.Show("Удалить "+costumesList.Values[selectedRow].getName()+"?", "", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    deleteCostume(selectedRow);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
